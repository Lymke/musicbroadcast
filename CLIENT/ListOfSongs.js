function ListOfSongs() { 

    /** ******************** ATTRIBUTES ***************** **/

    /**
     * 
     */
	this.aSongs = [];
        
    /** ******************** METHODES ***************** **/
    
    /**
     * 
     * @returns 
    */
	this.count = function(){
        return this.aSongs.length;
    };

    /**
     */
    this.addSong = function(oSong){

        if(this.aSongs[oSong.iId]  == undefined){

            this.aSongs[oSong.iId] = {
                oAudio : new Audio(),
                oSrc : document.createElement("source")
            };

            this.aSongs[oSong.iId].oSrc.type = "audio/mpeg";
            this.aSongs[oSong.iId].oSrc.src  = oSong.sSrc;
            this.aSongs[oSong.iId].oAudio.appendChild(this.aSongs[oSong.iId].oSrc);
            //this.aSongs.push(oSong);
        }
        
    };

    this.play = function(oSong){
        if(this.aSongs[oSong.iId]  == undefined){
            this.addSong(oSong);
        }
        this.aSongs[oSong.iId].play();
    }

    /**
     * 
     */
    this.subSong = function(iIdSong){
        for (p in this.aSongs) {
            if(this.aSongs[p].iId == iIdSong){
                this.aSongs.splice(iIdSong,1) 
            }
        }
    };
};