module.exports.controller = function (app) {
    
    io = app.get('io');

    /**
     * 
     * Don't forget : io.to(socketid).emit('message', 'whatever');
     */
    io.on('connection', function (socket) {

        socket.emit('message', 'Vous êtes bien connecté !');
        
       
        
        socket.on('music-play', function (oDatas){
            
            socket.broadcast.emit('music-play',oDatas);
                   
        });

        socket.on('music-pause', function (oDatas){
            
            socket.broadcast.emit('music-pause',oDatas);
                   
        });
        
        socket.on('disconnect', function () {

        });
    });

};